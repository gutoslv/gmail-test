# Gmail test
This repo is for the Gmail test.Cypress with Cucumber was not used since it adds extra complexity that is unnecessary in most cases. All the tests were written based on the Gherkin scenarios provided on the `./scenarios.feature` file.

## Tools used
I’m using [Cypress Testing Library](https://testing-library.com/docs/cypress-testing-library/intro/) instead of CSS selectors. The reason for that is that the Testing Library aims to provide a selector that doesn’t change when the implementation changes, only when the functionality of a part of the Web UI changes. This makes the tests easier to maintain once we get the hang of using those selectors.

## How to run the tests locally
First you’ll need [Node.js](https://nodejs.org/en/) and [npm](http://npmjs.com) installed.
Open a terminal on the root folder of this repo, and run `npm ci`; then, you can just run `npm run test:chrome` for testing on Chrome (not working due to Chrome blocking cross-origin frames) or `npm run test:firefox` to run the tests on Firefox. For both cases, you’ll need the browser installed on your machine.
The user and password need to be changed on the `./cypress.json` env.