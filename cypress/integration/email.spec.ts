describe('Email feature', function () {
  before(function () {
    // this whole before function could be refactored to not login through the UI
    // I'm only using the UI because I don't have access to the API/cookie setting way
    //that Google uses to check if user is logged in
    cy.visit('/');

    cy.findByRole('textbox', {
      name: /email or phone/i,
    }).type(Cypress.env('user'));
    cy.findByText(/next/i).click();

    cy.findByRole('password', {
      name: /password/i,
    }).type(Cypress.env('user'));
    cy.findByText(/next/i).click();
    cy.findByRole('button', {
      name: /compose/i,
    }).click();
  });

  it('should send an email', function () {
    cy.findByRole('button', {
      name: /compose/i,
    }).click();

    cy.findByRole('combobox', {
      name: /to/i,
    }).type('Egary1961@rhyta.com');

    cy.findByRole('textbox', {
      name: /subject/i,
    }).type('This is a test email');

    cy.findByRole('textbox', {
      name: /message body/i,
    }).type('An example content body');

    cy.findByRole('button', {
      name: /send/i,
    }).click();

    cy.findByRole('link', {
      name: /view message/i,
    }).should('be.visible');
  });

  it('should save a draft message', function () {
    cy.visit('/');

    cy.findByRole('button', {
      name: /compose/i,
    }).click();

    cy.findByRole('combobox', {
      name: /to/i,
    }).type('Egary1961@rhyta.com');

    cy.findByRole('textbox', {
      name: /subject/i,
    }).type('This is a draft email');

    cy.findByRole('textbox', {
      name: /message body/i,
    }).type('An example draft content body');

    cy.findByRole('img', {
      name: /save & close/i,
    }).click();

    cy.findByRole('link', {
      name: /drafts/i,
    }).click();

    cy.findByRole('row', {
      name: /draft/i,
    }).should('be.visible');
  });

  it('should navigate through tabs', function () {
    cy.visit('/');

    cy.findByRole('tab', {
      name: /social/i,
    }).click();

    cy.findByText(/your social tab is empty\./i).should('be.visible');
  });

  it('should see the starred messages panel', function () {
    cy.findByRole('link', {
      name: /starred/i,
    }).click();

    cy.findByRole('cell', {
      name: /no starred messages\. stars let you give messages a special status to make them easier to find\. to star a message, click on the star outline beside any message or conversation\./i,
    }).should('be.visible');
  });
});
