Feature: Email
  Background:
    Given I am logged in on Gmail
    And I am on the "Mail" page

  #This is the most basid functionality IMO, because the core of the app is to send emails
  Scenario: Send Email
    When I compose a new email
    And I send the new email to an email address
    Then I should see a popup informing that the email was sent
    And I should be able to unsend the message
    And I should be able to view the message that was sent

  # making sure that an email that was being composed is saved as a draft is important to prevent the user from losing
  # all that was wrote due to some external error (browser erros, blue screen, power outage)
  Scenario: Save a Draft Email
    When I compose a new email
    And I close the "New message" popup
    And I access the "Drafts" Tab
    Then the draft for the message should be saved

  #Both scenarios below could be improved by checking if the mails are being filtered properly but due to lack of
  #a reliable data mass, I've opted for not testing those scenarios, and only testing an empty scenario (the default for a new account)
  Scenario: Navigate through mail tabs
    Given I am at the Inbox
    When I click on the "Social" tab
    Then I should see the "Your Social tab is empty" message

  Scenario: Check starred message panel
    Given there is no starred message
    When I navigate to the Starred messages panel
    Then I should see a message informing that are no starred messages